<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use App\Imenik;

class DataController extends Controller
{

    public function getImenik($id)
    {
        if($user = JWTAuth::parseToken()->authenticate()) {
            $user_id = $id;
            $data = Imenik::query()
                ->where('user_id', $user_id)
                ->get();

            return response()->json($data, 200);
        }

    }

    public function insertImenik(Request $request){
        if($user = JWTAuth::parseToken()->authenticate()) {
            $user_id = $user->id;
            $data = Imenik::create([
                'ime_kontakta' => $request->get('ime_kontakta'),
                'telefon' => $request->get('telefon'),
                'email' => $request->get('email'),
                'user_id' => $user_id
            ]);

            if($data){
                return response()->json(['status' => 'OK', 'msg' => 'Uspesno ste kreirali novi kontakt.'],200);
            }else {
                return response()->json(['status' => 'Error', 'msg' => 'Greska'],400);
            }
        }
    }

    public function editImenik(Request $request){
        if($user = JWTAuth::parseToken()->authenticate()) {
            $id = $request->get('id');
            $imenik = Imenik::query()
                ->where('id', $id)
                ->where('user_id', $user->id)
                ->first();


            if($imenik){
                $imenik->ime_kontakta = $request->get('ime_kontakta');
                $imenik->telefon = $request->get('telefon');
                $imenik->email = $request->get('email');
                try {
                    $imenik->save();
                    return response()->json(['status' => 'OK', 'msq' => 'Uspesno ste izmenili kontakt.'],200);
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                    return response()->json(['status' => 'Error', 'msg' => 'Greska'],404);
                }
            }else {
                return response()->json(['status' => 'Error', 'msg' => 'Greska'],404);
            }


        }
    }

}
